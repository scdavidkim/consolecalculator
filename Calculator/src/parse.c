#include <stdio.h>			// for printf() function
#include <ctype.h>			// for tolower(), isspace() functions
#include <string.h>			// for strtok() function
#include <stdlib.h>			// for strtod(), strcmp() functions

#include "parse.h"

int checkFirstCharacter(char firstCharacter); 
int checkLastCharacter(char lastCharacter); 
int isNotOperand(char input);
int priority(char *input);
int push(char *input, char **stack, int top);
int pop(int top);
char *getTop(char **stack, int top);
int isStackEmpty(int top);
int isOpeningParentheses(char *input);
int isClosingParentheses(char *input);

void makeLowerCase(char *string)
{
	int i = 0;

	/* Go through each character in string and convert to lower case */
	while (string[i] != '\n') {
		string[i] = tolower(string[i]);
		i++;
	}
}

void removeSpaces(char *string)
{
	char *i = string;
	char *j = string;

	/* Copy elements from string j into string i, skipping any 		 */
	/* whitespaces 													 */ 
	while (*j) {
		*i = *j++;
		if (*i != ' ') {
			i++;
		}
	}
	
	/* Terminate the string appropriately */
	*i = 0;
}

int checkIfHelpOrQuit(char *userInput)
{
	switch (userInput[0]) {
		case 'h':
			return 1;		// Will cause help message to print from main
			break;

		case 'q':
			printf("Goodbye!\n");
			exit(0);		// Will exit program

		default:
			return 0;		// Will cause error message to print from main
	}
}

int checkOperators(char *userInput, int inputLength) 
{
	/* Check if the first character is any operator other than '-' */
	if (checkFirstCharacter(userInput[FIRST_CHARACTER]) == INVALID_INPUT) {
		return 0; 
	}
	
	/* Check if the last character is any operator */ 
	if (checkLastCharacter(userInput[inputLength - 1]) == INVALID_INPUT) {
		return 0; 
	}
	
	int i = 0; 
	while (userInput[i] != '\n') {
		
		/* Two operators cannot succeed one another unless the second */
		/* is a negative sign 										  */
		if (isOperatorChar(userInput[i])) {
			if (isOperatorChar(userInput[i + 1])) {
				if (userInput[i + 1] != '-') {
					return 0; 	// Will prompt main to print error message 
				}
			}

			/* No operator can be followed by a closing parentheses   */ 
			else if (userInput[i + 1] == ')') {
				return 0; 		// Will prompt main to print error message
			}
		}
		
		/* No operator can follow an opening bracket unless it is a   */
		/* negative sign 											  */
		else if (userInput[i] == '(') {
			if (isOperatorChar(userInput[i + 1])) {
				if (userInput[i + 1] != '-') {
					return 0; 
				}
			}
		}
		i++;
	}
	
	return 1; 
}

void addSpaces(char *userInput, char *spacedInput)
{
	int i = 0, j = 0;
	
	while (userInput[i] != '\n') {
		if (userInput[i] == '-') {
			
			/* If expression starts with '-', then it is a negative    */
			/* sign, so do not put a space between it and the 		   */ 
			/* following number.								  	   */
			if (i == FIRST_CHARACTER) {
				spacedInput[j] = userInput[i];
            	j++;
			}
			
			/* If value before '-' is an operand, put a space before   */
			/* and after it as it is a subtraction operator in this    */
			/* context. 							                   */ 
			else if (!isNotOperand(userInput[i-1])) {
				spacedInput[j] = ' ';
            	j++;
            	spacedInput[j] = userInput[i];
           		j++;
            	spacedInput[j] = ' ';
            	j++;
			}
			
			/* Otherwise, '-' is a negative sign and so place a space  */
			/* before it, but not after it (append it to the following */
			/* operand).							 				   */
			else {
				spacedInput[j] = ' ';
            	j++;
				spacedInput[j] = userInput[i];
            	j++;
			}
		}
		
		/* If current value is an operator or bracket, put spaces 	   */
		/* before and after it to separate it from surrounding 		   */
		/* operators and operands. 						 			   */ 
		else if (isNotOperand(userInput[i])) {
			spacedInput[j] = ' ';
            j++;
            spacedInput[j] = userInput[i];
            j++;
            spacedInput[j] = ' ';
            j++;
		}
		
		/* Otherwise, if the value is a number or '.', don't separate  */
		/* it from other surrounding numbers (required for multiple    */
		/* digits and floats).				 						   */
		else {
			spacedInput[j] = userInput[i];
            j++;
		}

		i++;
	}

}

void tokeniseExpression(char *userInput, char **infixExpression)
{
	char delimiters[] = " \n";
	int i = 0;

	/* Tokenise received expression at the specified delimiters, 	   */
    char *input = strtok(userInput, delimiters);

	/* Place current token in its own index of an array of pointers.   */
	while (input != NULL) {
		infixExpression[i++] = input;
		input = strtok(NULL, delimiters);
	}
}

int infixToPostfix(char **infixExpression, char **postfixExpression)
{
	char *stack[STACK_HEIGHT];
	int top = -1, i = 0, j = 0;

	while(infixExpression[i]) {
		
		/* All operands are simply appended to the postfix expression  */ 
		if (isFloat(infixExpression[i])) {
			postfixExpression[j] = infixExpression[i];
			j++;
		}
		
		/* If the operator at the top of the stack has a higher        */
		/* arithmetic precedence than the operator currently observed, */
		/* then this operator can be appended to postfix expression    */
		else if (isOperator(infixExpression[i])) {
			while(!isStackEmpty(top) && 
				  (priority(stack[top]) >= priority(infixExpression[i])) &&
				  !isOpeningParentheses(getTop(stack, top))) {
				postfixExpression[j] = getTop(stack, top);
				top = pop(top);
				j++;
			}
			
			/* The current operator can now be pushed onto the top of  */
			/* the stack. 											   */
			top = push(infixExpression[i], stack, top);
		}
		
		/* Opening parentheses are simply pushed onto the top of the   */
		/* stack. 													   */
		else if (isOpeningParentheses(infixExpression[i])) {
			top = push(infixExpression[i], stack, top);

		}
		
		/* If a closing bracket is spotted, append all the values in   */
		/* the stack onto the postfix expression, until an opening     */
		/* parentheses is spotted. 									   */
		else if (isClosingParentheses(infixExpression[i])) {
			while(!isStackEmpty(top) && 
				  !isOpeningParentheses(getTop(stack, top))) {
				postfixExpression[j] = getTop(stack, top);
				top = pop(top);
				j++;
			}
			top = pop(top);
		}
		else {
			return 0;	// Is neither an operator, operand or parentheses. 
		}
		i++;
	}
	
	/* All remaining operators in the stack can now be appended to the */
	/* postfix expression. 											   */
	while (!isStackEmpty(top)) {
		postfixExpression[j] = getTop(stack, top);
		top = pop(top);
		j++;
	}

	return 1;
}

int isOperatorChar(char input)
{
	char *operators = "+^-/*e";

	int i = 0;
	while (operators[i]) {
		if (input == operators[i]) {
			return 1;
		}
		i++;
	}

	return 0;
}

int checkFirstCharacter(char firstCharacter) 
{
	if (isOperatorChar(firstCharacter)) {
		
		/* If the first character is an operator, it must be a '-' */ 
		if (firstCharacter != '-') {
			 return 0; 
		 }
	}
	
	/* First character cannot be a closing parentheses.  */ 
	else if (firstCharacter == ')') {
		return 0; 
	}
	
	return 1;
}

int checkLastCharacter(char lastCharacter)
{
	/* Expression cannot end with an operator */ 
	if (isOperatorChar(lastCharacter)) {
		return 0;
	}
	
	/* Expression also cannot end with an opening bracket */
	else if (lastCharacter == '(') {
		return 0;
	}
	return 1; 
}

int isNotOperand(char input)
{
	char *operators = "+^-/*()e";

	int i = 0;
	
	/* If current input is any of the characters above, then return */
	/* true. 													    */
	while (operators[i]) {
		if (input == operators[i]) {
			return 1;
		}
		i++;
	}

	return 0;
}

int isFloat(char *input)
{
	char *endPointer;
	
	/* Convert string parameter to float, and receive the pointer for */
	/* where the conversion ended. 									  */
	strtod(input, &endPointer);

	/* If conversion ends at a space or zero, then must be a float */ 
  	if (isspace(*endPointer) || *endPointer == 0) {
    	return 1;
	}
  	else {
    	return 0;	// Calling function will prompt main to print error message
	}
}

int isOperator(char *input)
{
	char *operators = "+^-/*e";
	int i = 0;

	/* If current input is any of the operators above, then return */
	/* true. 													   */
	while (operators[i]) {
		if (*input == operators[i]) {
			return 1;
		}
		i++;
	}

	return 0;
}

int priority(char *input)
{
	/* Rank operators based on their precedence arithmetically */ 
	if(!strcmp(input, "+")) {
		return 1;
	}
	if(!strcmp(input, "-")) {
		return 1;
	}
	if(!strcmp(input, "*")) {
		return 2;
	}
	if(!strcmp(input, "/")) {
		return 2;
	}
	if (!strcmp(input, "^")) {
		return 3;
	}
	if (!strcmp(input, "e")) {
		return 4;
	}
	else {
		return 0;
	}
}

int push(char *input, char **stack, int top)
{
	/* Increment the top index and push new value to the top of */
	/* stack. 												    */
	top = top + 1;
	stack[top] = input;

	return top; 	
}

int pop(int top)
{
	/* Decrement top index, meaning the stack has reduced in   */
	/* "height" by 1 value. 								   */
	top = top - 1;
	
	return top; 
}

char *getTop(char **stack, int top)
{
	/* Send back the value at the very top of the stack. 	  */ 
	return stack[top];
}

int isStackEmpty(int top)
{
	/* A top index of -1 indicates that the stack is empty    */ 
	if (top == - 1) {
		return 1;
	}
	else {
		return 0;
	}
}

int isOpeningParentheses(char *input)
{
	if (!strcmp(input, "(")) {
		return 1;
	}
	else {
		return 0;
	}
}

int isClosingParentheses(char *input)
{
	if (!strcmp(input, ")")) {
		return 1;
	}
	else {
		return 0;
	}
}

