#include <stdio.h>					// for fgets(), printf() functions
#include <string.h>					// for strlen() function
#include <ctype.h>					// for isalpha() function

#include "parse.h"
#include "calc.h"

#define NO_QUIT 1                                                         
#define MAX_INPUT 100
#define SINGLE_CHARACTER_INPUT 2

void printWelcomeMessage(void);
void displayInputPrompt(void);
void getUserInput(char *userInput);
void printIllegalInputMessage(void);
void displayHelpMessage(void);
void divideByZeroMessage(void);

int main(void) 
{
	printWelcomeMessage();
	
	/* Keep the program running until the user decides to quit */
	while (NO_QUIT) {
		displayInputPrompt();
		
		/* Receive the user input, make it lower case and also   */
		/* remove all spaces                                     */
		char userInput[MAX_INPUT] = {0}; 
		getUserInput(userInput); 
		makeLowerCase(userInput);
		removeSpaces(userInput); 
		
		/* If only one character is received, check if it is for */
		/* help or quit functions                                */
		if (strlen(userInput) == SINGLE_CHARACTER_INPUT && 
			isalpha(userInput[FIRST_CHARACTER])) {
			if (checkIfHelpOrQuit(userInput) == INVALID_INPUT) {
				printIllegalInputMessage();
				continue; 
			}
			else {
				displayHelpMessage();
			}
		}
		
		/* For inputs larger than 1 character in length */ 
		else {
			int inputLength = strlen(userInput) - 1; 
			
			/* Check whether the user has used operators in a valid */ 
			/* manner   											*/
			if (checkOperators(userInput, inputLength) == INVALID_INPUT) {
				printIllegalInputMessage();
				continue; 
			}
			
			/* Appropriately space the user input to distinguish 	*/
			/* between operators and operands 						*/
			char spacedInput[MAX_INPUT] = {0};
			addSpaces(userInput, spacedInput); 
			
			/* Tokenise user expression into operators and operands */
			char *infixExpression[MAX_INPUT] = {NULL};
			tokeniseExpression(spacedInput, infixExpression);
			
			/* Convert user infix expression into postfix notation  */
			char *postfixExpression[MAX_INPUT] = {NULL};
			if (infixToPostfix(infixExpression, postfixExpression) == INVALID_INPUT) {
				printIllegalInputMessage();
				continue;
			}
			
			/* Evaluate postfix expression and return numerical 	*/
			/* result 												*/ 
			double result = evaluatePostfix(postfixExpression);
			
			/* Check if the user has entered an expression that 	*/
			/* requires division by zero 							*/
			if (result == DIVIDE_BY_ZERO) {
				divideByZeroMessage();
				continue;
			}
			
			/* Otherwise, print the result to the screen */
			else {
				printf("%.3lf\n", result);
			}
		}
	}
}

void printWelcomeMessage(void)
{
	printf("Simple Calculator\n");
}

void displayInputPrompt(void)
{
	printf(">>> ");
}

void getUserInput(char *userInput)
{
	fgets(userInput, MAX_INPUT, stdin);
}

void printIllegalInputMessage(void)
{
	printf("Error: Illegal input!\n");
}

void displayHelpMessage(void)
{
	printf("Simple Calculator understands the following arithmetic operations:\n");
	printf("^ exponentiation\n");
	printf("+ addition\n");
	printf("- subtraction\n");
	printf("* multiplication\n");
	printf("/ division\n");
}

void divideByZeroMessage(void)
{
	printf("Error: Divide by zero!\n");
}

