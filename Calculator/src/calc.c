#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "calc.h"
#include "parse.h"

int pushDouble(double input, double *stack, int top);
double popDouble(int *top, double *stack);
double performArithmetic(char *operator, double operand1, double operand2);

double evaluatePostfix(char **postfixExpression)
{
	int top = -1;
	int i = 0;
	double stack[STACK_HEIGHT], operand1, operand2, result;
	char *operator;

	while(postfixExpression[i]) {

		/* Disregard all stray newlines in the expression */
		if (!strcmp(postfixExpression[i], "\n")) {
			i++;
			continue;
		}
		
		/* If the current "object" is a float, convert it to double and push */
		/* it onto stack 													 */
		else if (isFloat(postfixExpression[i])) {
			top = pushDouble(atof(postfixExpression[i]), stack, top);
		}
		
		/* If the current "object" is an operator, store it in the operator */
		/* variable, and pop the previous two operand values from the stack */
		/* storing these in variables as well. 								*/
		else if(isOperator(postfixExpression[i])) {
			operator = postfixExpression[i];
			operand2 = popDouble(&top, stack);
			operand1 = popDouble(&top, stack);

			/* Perform the arithmetic with the given operands and operator, */
			/* and return the result 										*/
			result = performArithmetic(operator, operand1, operand2);

			/* If we have divided by zero, return an error code to the main */
			if (result == DIVIDE_BY_ZERO) {
				return DIVIDE_BY_ZERO;
			}
			
			/* Otherwise, store push this new result to the top of the      */
			/* stack  														*/ 
			else {
				top = pushDouble(result, stack, top);
			}
		}
		i++;
	}
	
	/* By here, the number at the top of the stack is the final result, so  */
	/* return it to main.													*/
	return popDouble(&top, stack);
}

int pushDouble(double input, double *stack, int top)
{
	top = top + 1;			// Increment value for the higest index of stack.
	stack[top] = input;		// Store the given value parameter at the top index of stack.
	return top;
}

double popDouble(int *top, double *stack)
{
	double returnValue = stack[*top];
	*top = *top - 1;
	return returnValue;		// Return value at top of stack and decrement index. 
}

double performArithmetic(char *operator, double operand1, double operand2)
{
	double result;

	/* Check the input operator parameter, and perform the appropriate 	*/
	/* arithmetic with the given operands. Return this value. 			*/
	if (!strcmp(operator, "+")) {
		result = operand1 + operand2;
	}
	
	else if (!strcmp(operator, "-")) {
		result = operand1 - operand2;
	}
	
	else if (!strcmp(operator, "*")) {
		result = operand1 * operand2;
	}
	
	else if (!strcmp(operator, "/")) {
		if (operand2 == 0) {
			return DIVIDE_BY_ZERO;	// Return an error signal. 
		}
		result = operand1 / operand2;
	}
	
	else if (!strcmp(operator, "^")) {
		result = pow(operand1, operand2);
	}
	
	else if (!strcmp(operator, "e")) {
		result = operand1 * pow(10, operand2);
	}

	return result;
}

