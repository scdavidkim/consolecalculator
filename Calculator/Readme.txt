This console program calculates and displays the result of arbitrary mathematical expressions entered by the user.
It converts the input string into postfix notation, before evaluating the result in a serious of stack operations.

1. When the program is run, it displays a short welcome message.

	Simple Calculator

2. The program displays a simple prompt waiting for user input.

	>>>

3. The following help message is displayed whenever the user types the letter h at the input prompt.

	Simple Calculator understands the following arithmetic operations:
	^ exponentiation
	+ addition
	- subtraction
	* multiplication
	/ division

4. The program exits if the user types the letter q at the input prompt. Before exiting it prints the following message:

	Goodbye!

5. The program is able to perform calculations with the basic arithmetic operators (+, -, *, /), and exponentiation (^).

6. The program accepts decimals as well as integers.

7. The program accepts negative numbers.

8. The program accepts floating point numbers in scientific e notation - e.g. 1.23e4.

9. The program applies correct order of operations to calculations.

10. The program parses parentheses in the input string - ().

11. Arbitrary whitespace is allowed (but not required) in user input.

12. The program responds to all inputs in a case insensitive manner. i.e. any letters are valid in both upper and lower case.

13. The program validates user input, and recover gracefully from illegal input. Since a user might enter any arbitrary string of characters, many different errors are possible, including:

	a. Specifying two operators in a row (you must differentiate between the subtraction operator and the negative sign).

	b. Entering invalid letters.

	c. Entering a floating point number with more than one decimal point.

	d. Finishing the input with an operator rather than an operand.

14. On detection of illegal input, the following error message will be displayed:

	Error: Illegal input!

15. On detection of divide-by-zero, the following error message will be displayed:

	Error: Divide by zero!

16. The program will display the calculated result to 3 decimal places, with trailing zeros if required.

17. After displaying the calculated result or an error message, the program will return to the input prompt (see Item 2 above).

	Example Output
	Simple Calculator
	>>> 1 + 2 * 3 + 4
	11.000
	>>> hello
	Error: Illegal input!
	>>> 2 / 0
	Error: Divide by zero!
	>>> q

