/***************************************************************************
 * This module is a collection of functions that will seek to evaluate a   *
 * postfix arithmetic expression, with appropriate order of operations     *
 ***************************************************************************/

#ifndef CALC
#define CALC

#define DIVIDE_BY_ZERO 1234567

double evaluatePostfix(char **postfixExpression);

#endif

