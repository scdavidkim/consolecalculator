/***************************************************************************
 * This module is a collection of functions that appropriately tokenise a  *
 * given infix expression into operators and operands, whilst also 		   *
 * intelligently distinguishing between negative signs and subtraction     *
 * operators. It identifies any flaws in the arithmetic expression, but    *
 * valid expressions are then converted into postfix notation for future   *
 * evaluation. 						      								   *
 ***************************************************************************/

#ifndef PARSE
#define PARSE

/* Macros shared by multiple files */ 
#define FIRST_CHARACTER 0 
#define INVALID_INPUT 0
#define STACK_HEIGHT 100 

/****************************************************************************
 * makeLowerCase: Converts all the letter's of parameter string into lower  *
 *			  	  case. Returns nothing. 	             	    			*
 ****************************************************************************/
void makeLowerCase(char *string);

/****************************************************************************
 * removeSpaces: Removes the presence of all whitespaces from the string    *
 *				 parameter. Returns nothing. 								*
 ****************************************************************************/
void removeSpaces(char *string);

/****************************************************************************
 * checkIfHelpOrQuit: Check's if user input string requests help message to * 
 *					  be displayed, or program to be exited. If help,       *
 *					  function returns 1, if quit, program is exited. If an *
 *					  invalid input, returns 0. 							*
 ****************************************************************************/
int checkIfHelpOrQuit(char *userInput);

/****************************************************************************
 * checkOperators: Receives the userInput string and its length to go 		*
 *				   the expression and check if operators have been utilised *
 *				   in a valid manner. For example, two operators cannot 	*
 *				   succeed one another (unless second is a negative sign)   *
 *				   and expression cannot end with an operator. Returns 0 	*
 *				   if invalid input is detected. Returns 1 if all is fine. 	*
 ****************************************************************************/
int checkOperators(char *userInput, int inputLength);

/****************************************************************************
 * isOperatorChar: Receives a character input, and returns 1 if it is an	*
 *				   arithmetic operator, and returns 0 if it is not. 		*
 ****************************************************************************/
int isOperatorChar(char input);

/****************************************************************************
 * isOperator: Performs the same function as isOperatorChar, but for string *
 *			   parameters, as opposed to char. Returns 1 if the string is   *
 * 			   an arithmetic operator, and 0 if otherwise. 					*
 ****************************************************************************/
int isOperator(char *input);

/****************************************************************************
 * addSpaces:  Goes through user input string parameter, and copies the 	*
 *			   contents of this string into a new string, whose pointer is  *
 *			   a parameter to the function. The string is copied however 	*
 *			   with spaces added in appropriate places, so that the 		*
 * 			   resulting string is an arithmetic expression with single     *
 * 			   space delimiters. Returns nothing.							*
 ****************************************************************************/
void addSpaces(char *userInput, char *spacedString);

/*****************************************************************************
 * tokeniseExpression: Separates input string into tokens based on specified *
 * 					   delimiter argument. Tokenised strings are then stored *
 *					   in an array, whose pointer is received as a parameter *
 *					   Function returns nothing.						     *
 *****************************************************************************/
void tokeniseExpression(char *userInput, char **infixExpression);

/*****************************************************************************
 * infixToPostfix: Converts the infix expression parameter into a postfix    *
 * 				   expression. Each element is stored in an array, whose 	 *
 *				   pointer is received as a parameter. Function returns 	 *
 *				   nothing. 											     *
 *****************************************************************************/
int infixToPostfix(char **infixExpression, char **postfixExpression);


/*****************************************************************************
 * isFloat: Checks whether the input string parameter is of a float or not.  *
 *			Returns 1 if true, 0 if false. 									 *
 *****************************************************************************/
int isFloat(char *input);

#endif
